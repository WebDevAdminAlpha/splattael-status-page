import 'document-register-element';
import { emojiImageTag, emojiFallbackImageSrc } from './emoji';
import { isEmojiUnicodeSupportedByBrowser } from './emoji/support';

class GlEmoji extends HTMLElement {
  constructor() {
    super();
    const emojiUnicode = this.textContent.trim();
    const { name, unicodeVersion, fallbackSrc } = this.dataset;

    const isEmojiUnicode =
      this.childNodes &&
      Array.prototype.every.call(this.childNodes, childNode => childNode.nodeType === 3);
    const hasImageFallback = fallbackSrc && fallbackSrc.length > 0;

    if (
      emojiUnicode &&
      isEmojiUnicode &&
      !isEmojiUnicodeSupportedByBrowser(emojiUnicode, unicodeVersion)
    ) {
      if (hasImageFallback) {
        this.innerHTML = emojiImageTag(name, fallbackSrc);
      } else {
        const src = emojiFallbackImageSrc(name);
        this.innerHTML = emojiImageTag(name, src);
      }
    }
  }
}

export function installGlEmojiElement() {
  if (!customElements.get('gl-emoji')) {
    customElements.define('gl-emoji', GlEmoji);
  }
}
