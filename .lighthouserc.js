/* eslint-disable filenames/match-regex */

module.exports = {
  ci: {
    collect: {
      startServerCommand: 'ENV=production yarn serve',
      url: ['http://localhost:8080'],
      settings: { chromeFlags: '--no-sandbox' },
    },
    upload: {
      target: 'temporary-public-storage',
    },
  },
};
